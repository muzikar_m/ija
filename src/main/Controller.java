//Projekt IJA duben 2019 
//Autori:	xtelma00 
//		xmuzik06 

package main;

import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ListChangeListener;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.text.TextAlignment;
import javafx.stage.FileChooser;
import javafx.util.Duration;
import main.model.*;

import java.io.File;
import java.io.FileWriter;
import java.io.StringWriter;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class Controller {

    @FXML
    TabPane tabPane;

    @FXML
    GridPane chessboard;

    @FXML
    ListView undos;

    @FXML
    Label speedLabel;

    @FXML
    Slider speedSlider;

    Button activeButton;

    Button chessButtons[][];

    Timeline timeline;

    @FXML
    Button buttonPlay;

    @FXML
    Label turnLabel;

    @FXML
    Label sachLabel;

    /**
     * Inicializuje herni plochu
     */
    public void initialize(){
        chessButtons = new Button[8][8];
        tabPane.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Tab>() {
            @Override
            public void changed(ObservableValue<? extends Tab> observable, Tab oldValue, Tab newValue) {
                if (oldValue != null)
                    oldValue.setContent(null);
                chessboard.setDisable(false);
                newValue.setContent(chessboard);
                GameManager.getManager().setCurrentGame((int)newValue.getUserData());
                drawGame(GameManager.getManager().getCurrentGame());
            }
        });
        //labely
        //Tvorba radku
        for (int y = 0; y < 8; y++){
            Label l = new Label(Integer.toString(y + 1));
            l.setAlignment(Pos.CENTER);
            l.setMinWidth(50);
            l.setTextAlignment(TextAlignment.CENTER);
            chessboard.add(l, 0, y);
        }
        //Tvorba sloucpu
        for (int x = 1; x <= 8; x++){
            Label l = new Label("" + (char)('a' + x - 1));
            l.setAlignment(Pos.CENTER);
            l.setMinWidth(50);
            l.setTextAlignment(TextAlignment.CENTER);
            chessboard.add(l, x, 8);
        }
        for (int row = 0; row < 8; row++){
            for (int col = 0; col < 8; col++){
                Button btn = new Button(" ");
                chessButtons[7 - row][col] = btn;
                btn.getStyleClass().add("chessbutton");
                btn.setMinSize(50, 50);
                btn.centerShapeProperty().set(true);
                btn.setTextAlignment(TextAlignment.CENTER);
                if (row % 2 == 0){
                    if (col % 2 == 0){
                        btn.getStyleClass().add("brown");
                    } else {
                        btn.getStyleClass().add("white");
                    }
                } else {
                    if (col % 2 == 1){
                        btn.getStyleClass().add("brown");
                    } else {
                        btn.getStyleClass().add("white");
                    }
                }
                btn.setUserData(new Address(col, 7 - row));
                chessboard.add(btn, col + 1, row);
                btn.setOnMouseClicked(this::onGridClicked);
            }
        }
        undos.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        undos.getSelectionModel().getSelectedIndices().addListener(new ListChangeListener() {
            @Override
            public void onChanged(Change c) {
                if (c.next()){
                    if (c.wasAdded()){
                        System.out.println(c);
                        Game game = GameManager.getManager().getCurrentGame();
                        int move = (int)c.getList().get(0);
                        game.setCurrentMove(move);
                        Platform.runLater(() -> drawGame(game));
                    }
                }
            }
        });
        speedSlider.valueProperty().addListener((observable, oldValue, newValue) -> {
            speedLabel.setText(String.format("Rychlost prehravani: %d ms", Math.round(speedSlider.getValue())));
        });
        speedSlider.valueProperty().setValue(speedSlider.getMax() / 2);
        //drawGame(GameManager.getManager().getCurrentGame());
    }

    /**
     * Vykresli hru
     * @param game aktualni hra pro vykresleni
     */
    public void drawGame(Game game){
        Board b = game.getBoard();
        for (int row = 0; row < 8; row++){
            for (int col = 0; col < 8; col++){
                Field f = b.getField(row,col);
                if (f.hasPiece()){
                    chessButtons[row][col].setText(f.getPiece().getStringRepr());
                } else {
                    chessButtons[row][col].setText(" ");
                }
            }
        }
        drawUndos(game);
        turnLabel.setText(game.isBlacksTurn()?"Hraje cerny":"Hraje bily");
        sachLabel.setText(game.isSach()?"Sach":"");
    }

    /**
     * Vykrasli seznam pohybu
     * @param game aktualni hra pro kterou vykreslovat pohyby
     */
    public void drawUndos(Game game){
        List<Move> moves = game.getMoves();
        undos.getItems().clear();
        for (int i = 0; i < moves.size(); i++){
            undos.getItems().add(moves.get(i).getFullNotation());
        }
    }


    public void onGridClicked(Event e){
        Button b = (Button)e.getSource();
        if (GameManager.getManager().getCurrentGameIndex() == -1){
            return;
        }
        Game game = GameManager.getManager().getCurrentGame();
        Board board = game.getBoard();
        Field f = board.getField((Address)b.getUserData());
        if (activeButton != null){
            Address toA = (Address)b.getUserData();
            activeButton.getStyleClass().remove("chessbutton-active");
            Address fromA = (Address)activeButton.getUserData();
            Field from = board.getField(fromA);
            Field to = board.getField(toA);
            if (from.hasPiece()){
                if (from.getPiece().canMove(toA.getCol(), toA.getRow())){
                    game.doMove(Move.fromFields(from, to));
                }
            }
            activeButton = null;
            chessboard.lookupAll(".move-move").forEach(node -> node.getStyleClass().remove("move-move"));
            chessboard.lookupAll(".move-kill").forEach(node -> node.getStyleClass().remove("move-kill"));
        } else {
            if (!f.hasPiece() || (f.getPiece().isBlack() != game.isBlacksTurn())){
                return;
            }
            activeButton = b;
            b.getStyleClass().add("chessbutton-active");
            Address a = (Address)b.getUserData();
            Field field = board.getField(a);
            if (field.hasPiece()){
                List<PossibleMove> possibleMoves = field.getPiece().getPossibleMoves();
                for (PossibleMove m : possibleMoves){
                    System.out.println(String.format("[%d,%d] + " + m.type, m.row, m.col));
                    System.out.println(m);
                    if (m.type == PossibleMove.MoveType.MOVE)
                        chessButtons[m.row][m.col].getStyleClass().add("move-move");
                    else
                        chessButtons[m.row][m.col].getStyleClass().add("move-kill");
                }
            }
        }
        drawGame(GameManager.getManager().getCurrentGame());
    }

    public void debugButton(){
        drawGame(GameManager.getManager().getCurrentGame());
    }

    public void undoButton(){
        Game game = GameManager.getManager().getCurrentGame();
        game.undo();
        drawGame(game);
    }

    public void redoButton(){
        Game game = GameManager.getManager().getCurrentGame();
        game.redo();
        drawGame(game);
    }

    public void playButton(){
        if (timeline == null){
            timeline = new Timeline();
        }
        if (timeline.statusProperty().get().equals(Animation.Status.RUNNING)){
            buttonPlay.setText("Play");
            timeline.pause();
            speedSlider.setDisable(false);
        } else {
            buttonPlay.setText("Stop");
            speedSlider.setDisable(true);
            long timeout = Math.round(speedSlider.getValue());
            timeline = new Timeline();
            timeline.setCycleCount(Timeline.INDEFINITE);
            timeline.getKeyFrames().add(new KeyFrame(Duration.millis(timeout),
                    event -> redoButton()));
            timeline.playFromStart();
        }
    }

    public void saveButton(){
        FileChooser chooser = new FileChooser();
        File f = chooser.showSaveDialog(null);
        try {
            FileWriter writer = new FileWriter(f);
            List<Move> moves = GameManager.getManager().getCurrentGame().getMoves();
            for (int i = 0; i < moves.size() / 2; i++){
                writer.append(String.format("%d. ", i + 1));
                writer.append(moves.get(i * 2).getFullNotation());
                writer.append(' ');
                writer.append(moves.get(i * 2 + 1).getFullNotation());
                writer.append('\n');
            }
            if (moves.size() % 2 == 1){
                writer.append(String.format("%d. ", (moves.size() / 2) + 1));
                writer.append(moves.get(moves.size() - 1).getFullNotation());
                writer.append('\n');
            }
            writer.close();
        } catch (Exception e){
            e.printStackTrace();
        }

    }

    public void loadButton(){
        FileChooser chooser = new FileChooser();
        File file = chooser.showOpenDialog(null);
        try {
            GameManager gameManager = GameManager.getManager();
            Game game = new Game();
            game.loadFile(file);
            int id = gameManager.newGame(game);

            Tab tab = new Tab(file.getName());
            tab.setUserData(id);
            tabPane.getTabs().add(tab);
            tabPane.getSelectionModel().select(tab);
            drawGame(gameManager.getCurrentGame());
        } catch (Exception e){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setContentText(e.getMessage());
            alert.showAndWait();
        }
    }

    public void newGameButton(){
        GameManager gameManager = GameManager.getManager();
        Game newgame = new Game();
        int id = gameManager.newGame(newgame);
        Tab tab = new Tab("Untitled game");
        tab.setUserData(id);
        tabPane.getTabs().add(tab);
        tabPane.getSelectionModel().select(tab);

        drawGame(gameManager.getCurrentGame());
    }


}
