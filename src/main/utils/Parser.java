//Projekt IJA duben 2019 
//Autori:	xtelma00 
//		xmuzik06 

package main.utils;

import javafx.util.Pair;
import main.model.*;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Parser {

    private BufferedReader reader;
    private int currentMove;
    private Board board;
    private boolean canRead;

    private static final Pattern FULL_NOTATION = Pattern.compile("(\\d+). ([KDVSJp]?)([a-h])(\\d)([a-h])(\\d)(?: ([KDVSJp]?)([a-h])(\\d)([a-h])(\\d))?");
    private static final Pattern SHORT_NOTATION = Pattern.compile("(\\d+). ([KDVSJp]?)([a-h])(\\d)(?: ([KDVSJp]?)([a-h])(\\d))?");
    private static final Pattern IGNORE_LINE = Pattern.compile("\\w*");

    /**
     * Vytvori parser pro cteni notace sachovych her
     * @param file soubor ze ktereho cist data
     * @param board deska se kterou ma parser pracovat
     * @throws Exception vyjimka v pripade spatne prace se souborem
     */
    public Parser(File file, Board board) throws Exception {
        this.reader = new BufferedReader(new FileReader(file));
        this.board = board;
        this.currentMove = 0;
        canRead = true;
    }

    /**
     * Muze parser poskytovat dalsi pohyby
     * @return true pokud lze jeste cist
     */
    public boolean hasNext(){
        return canRead;
    }

    private Pair<Move, Move> parseFullNotation(Matcher matcher){
        int index = Integer.parseInt(matcher.group(1));
        if (index != currentMove){
            throw new RuntimeException("Zadan spatny index pohybu");
        }
        char figure1 = 'p';
        if (matcher.group(2).length() != 0){
            figure1 = matcher.group(2).charAt(0);
        }
        char col1 = (char)(matcher.group(3).charAt(0) - 'a');
        int row1 = Integer.parseInt(matcher.group(4)) - 1;
        Field f1 = board.getField(row1, col1);
        if (f1.isEmpty()){
            throw new RuntimeException("Pohyb s figurkou na spatnem poli");
        }
        Piece p1 = f1.getPiece();
        if (!p1.getType().equals(figure1)){
            throw new RuntimeException("Spatne oznacena figurka");
        }

        char col2 = (char)(matcher.group(5).charAt(0) - 'a');
        int row2 = Integer.valueOf(matcher.group(6)) - 1;

        Field f2 = board.getField(row2, col2);

        if (!p1.canMove(col2, row2)){
            throw new RuntimeException("Zadana figurka nemuze provest zadany pohyb");
        }

        if (matcher.group(8) != null){
            char figure2 = 'p';
            if (matcher.group(7).length() != 0){
                figure2 = matcher.group(7).charAt(0);
            }
            char col3 = (char)(matcher.group(8).charAt(0) - 'a');
            int row3 = Integer.parseInt(matcher.group(9)) - 1;
            Field f3 = board.getField(row3, col3);
            if (f3.isEmpty()){
                throw new RuntimeException("Pohyb s figurkou na spatnem poli");
            }
            Piece p2 = f3.getPiece();
            if (!p2.getType().equals(figure2)){
                throw new RuntimeException("Spatne oznacena figurka");
            }

            char col4 = (char)(matcher.group(10).charAt(0) - 'a');
            int row4 = Integer.valueOf(matcher.group(11)) - 1;

            if (!p2.canMove(col4, row4)){
                throw new RuntimeException("Zadana figurka nemuze provest zadany pohyb");
            }

            Field f4 = board.getField(row4, col4);

            return new Pair<>(Move.fromFields(f1,f2), Move.fromFields(f3, f4));
        }
        return new Pair<>(Move.fromFields(f1,f2), Move.makeEmpty());
    }

    private Pair<Move, Move> parseShortNotation(Matcher matcher){
        int index = Integer.parseInt(matcher.group(1));
        if (index != currentMove){
            throw new RuntimeException("Zadan spatny index pohybu");
        }
        char figure1 = 'p';
        if (matcher.group(2).length() != 0){
            figure1 = matcher.group(2).charAt(0);
        }
        char col1 = (char)(matcher.group(3).charAt(0) - 'a');
        int row1 = Integer.parseInt(matcher.group(4)) - 1;
        Field f1 = board.getField(row1, col1);
        final char fig = figure1;
        List<Pair<Piece, Stream<PossibleMove>>> possibleMoves = Arrays.stream(board.getPieces())
                .filter(piece -> piece.getType().equals(fig))
                .map(piece -> new Pair<>(piece, piece.getPossibleMoves().stream()))
                .filter(pair ->
                    pair.getValue().anyMatch(possibleMove -> possibleMove.row == row1 && possibleMove.col == col1)
                )
                .collect(Collectors.toList());
        if (possibleMoves.size() != 1){
            throw new RuntimeException("Nejednoznacny zapis kratke notace");
        }

        if (matcher.group(6) != null){
            Move m1 = Move.fromFields(possibleMoves.get(0).getKey().getField(), f1);
            char figure2 = 'p';
            if (matcher.group(5).length() != 0){
                figure2 = matcher.group(5).charAt(0);
            }
            char col2 = (char)(matcher.group(6).charAt(0) - 'a');
            int row2 = Integer.parseInt(matcher.group(7)) - 1;
            Field f2 = board.getField(row1, col1);
            final char fig2 = figure2;
            possibleMoves = Arrays.stream(board.getPieces())
                    .filter(piece -> piece.getType().equals(fig2))
                    .map(piece -> new Pair<>(piece, piece.getPossibleMoves().stream()))
                    .filter(pair ->
                            pair.getValue().anyMatch(possibleMove -> possibleMove.row == row2 && possibleMove.col == col2)
                    )
                    .collect(Collectors.toList());
            if (possibleMoves.size() != 1){
                throw new RuntimeException("Nejednoznacny zapis kratke notace");
            }
            return new Pair<>(m1, Move.fromFields(possibleMoves.get(0).getKey().getField(), f2));

        }

        return new Pair<>(Move.fromFields(possibleMoves.get(0).getKey().getField(), f1), Move.makeEmpty());
    }

    /**
     * Vraci dalsi dvojici pohybu ze souboru (muze se stat ze 2. pohyb neexistuje (vraci se Empty move)
     * @return dvojici pohybu
     * @throws Exception v pripade spatnych pohybu/spatne notace
     */
    public Pair<Move, Move> nextMove() throws Exception{
        String line = reader.readLine();
        if (line == null){
            canRead = false;
            return new Pair<>(Move.makeEmpty(), Move.makeEmpty());
        }
        currentMove++;
        Matcher matcher = FULL_NOTATION.matcher(line);
        if (matcher.matches()){
            return parseFullNotation(matcher);
        }
        matcher = SHORT_NOTATION.matcher(line);
        if (matcher.matches()){
            return parseShortNotation(matcher);
        }
        matcher = IGNORE_LINE.matcher(line);
        if (matcher.matches()){
            return new Pair<>(Move.makeEmpty(), Move.makeEmpty());
        }
        throw new RuntimeException(String.format("%s nesplnuje zadnou notaci", line));
    }
}
