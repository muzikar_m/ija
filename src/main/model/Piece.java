//Projekt IJA duben 2019 
//Autori:	xtelma00 
//		xmuzik06 

package main.model;

import main.model.pieces.*;

import java.util.ArrayList;
import java.util.List;

public abstract class Piece {

    Field field;
    Board board;
    boolean isBlack;

    public Piece(boolean isBlack, Board b){
        this.isBlack = isBlack;
        this.board = b;
    }

    public void setField(Field f){
        this.field = f;
    }

    public Field getField(){
        return this.field;
    }

    public Board getBoard(){
        return board;
    }

    /**
     * Funkce ktera implementuje pro kazdou figurku funkci pro kontrolu
     * validity pohybu s figurkou
     * @param col sloupec (rozmezi 0 - 7)
     * @param row radek (rozmezi 0 - 7)
     * @return true pokud figurka muze provest dany pohyb
     */
    public abstract boolean canMove(int col, int row);

    /**
     * Funkce vracejici vsehny validni pohyby pro danou figurku
     * @return list validnich pohybu
     */
    public List<PossibleMove> getPossibleMoves() {
        List<PossibleMove> moves = new ArrayList<>();

        for (int row = 0; row < 8; row++){
            for (int col = 0; col < 8; col++){
                if (canMove(col, row)){
                    PossibleMove.MoveType type = getBoard().getField(row, col).isEmpty() ? PossibleMove.MoveType.MOVE : PossibleMove.MoveType.KILL;
                    moves.add(new PossibleMove(type, row, col));
                }
            }
        }
        return moves;
    }

    public boolean isBlack(){
        return isBlack;
    }

    public abstract PieceType getType();

    public abstract String getStringRepr();

    /**
     * Tovarni funkce pro tvoreni typu figurek
     * @param type typ
     * @param isBlack je figurka cerna?
     * @param board deska na ktere bude figurka
     * @return figurku
     */
    public static Piece createPiece(PieceType type, boolean isBlack, Board board){
        switch (type) {
            case VEZ:
                return new Vez(isBlack,board);
            case DAMA:
                return new Dama(isBlack,board);
            case KRAL:
                return new Kral(isBlack,board);
            case PESEC:
                return new Pesec(isBlack,board);
            case JEZDEC:
                return new Jezdec(isBlack,board);
            case STRELEC:
                return new Strelec(isBlack,board);
        }
        return null;
    }

    public void setBoard(Board board) {
        this.board = board;
    }

    public void setBlack(boolean black) {
        isBlack = black;
    }

    /**
     * Typ figurek, obsahuje jeden parametr a to oznaceni v notaci
     */
    public enum PieceType {
        KRAL('K'),
        DAMA('D'),
        STRELEC('S'),
        VEZ('V'),
        PESEC('p'),
        JEZDEC('J');

        char n;

        PieceType(char notation){
            n = notation;
        }

        public boolean equals(char notation){
            return n == notation;
        }
    }
}
