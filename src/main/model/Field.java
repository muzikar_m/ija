//Projekt IJA duben 2019 
//Autori:	xtelma00 
//		xmuzik06 

package main.model;

public class Field {

    Piece piece;

    int col, row;

    /**
     * Jedno policko herni plochy
     * @param row radek
     * @param col sloupec
     */
    public Field(int row, int col) {
        this.col = col;
        this.row = row;
    }

    public boolean isEmpty(){
        return piece == null;
    }

    public boolean hasPiece(){
        return piece != null;
    }

    public void setPiece(Piece p){
        this.piece = p;
        if (p != null){
            p.setField(this);
        }
    }

    public Piece getPiece(){
        return piece;
    }

    public int getCol(){
        return col;
    }

    public int getRow(){
        return row;
    }
}
