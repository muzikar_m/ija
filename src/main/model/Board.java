//Projekt IJA duben 2019 
//Autori:	xtelma00 
//		xmuzik06 

package main.model;

import java.util.Arrays;
import java.util.List;

import static main.model.Piece.PieceType;

public class Board {

    public static final int BOARD_SIZE = 8;

    Field[][] fields;
    Piece[] pieces;
    int pieceCnt;

    /**
     * Vytvori a inicializuje novou herni desku
     */
    public Board(){
        pieceCnt = 0;
        pieces = new Piece[Board.BOARD_SIZE * 4];
        fields = new Field[BOARD_SIZE][BOARD_SIZE];
        for (int row = 0; row < BOARD_SIZE; row++){
            for (int col = 0; col < BOARD_SIZE; col++){
                fields[row][col] = new Field(row, col);
            }
        }
    }

    /**
     * Vytvori piece a ulozi si ji do cache
     */
    Piece makePiece(Piece.PieceType type, boolean isBlack, Board board){
        pieces[pieceCnt++] = Piece.createPiece(type, isBlack, board);
        return pieces[pieceCnt-1];
    }

    void initRow(int row, boolean isBlack){
        getField(row ,0).setPiece(makePiece(PieceType.VEZ, isBlack, this));
        getField(row ,BOARD_SIZE-1).setPiece(makePiece(PieceType.VEZ, isBlack, this));

        getField(row ,1).setPiece(makePiece(PieceType.JEZDEC, isBlack, this));
        getField(row ,BOARD_SIZE-2).setPiece(makePiece(PieceType.JEZDEC, isBlack, this));

        getField(row ,2).setPiece(makePiece(PieceType.STRELEC, isBlack, this));
        getField(row ,BOARD_SIZE-3).setPiece(makePiece(PieceType.STRELEC, isBlack, this));

        getField(row ,3).setPiece(makePiece(PieceType.KRAL, isBlack, this));
        getField(row ,BOARD_SIZE-4).setPiece(makePiece(PieceType.DAMA, isBlack, this));
    }

    /**
     * Nasadi figurky na herni desku podle pravidel sachu
     */
    public void initBoard(){
        for (int i = 0; i < BOARD_SIZE; i++){
            getField(1,i).setPiece(makePiece(PieceType.PESEC, false, this));
            getField(BOARD_SIZE-2,i).setPiece(makePiece(PieceType.PESEC, true, this));
        }
        initRow(0, false);
        initRow(BOARD_SIZE-1, true);
    }

    public Field getField(int row, int col){
        return fields[row][col];
    }

    public Field getField(Address a){
        return getField(a.getRow(),a.getCol());
    }

    public Piece[] getPieces() {
        return pieces;
    }
}
