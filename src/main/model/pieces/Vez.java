//Projekt IJA duben 2019 
//Autori:	xtelma00 
//		xmuzik06 

package main.model.pieces;

import main.model.*;

import java.util.ArrayList;
import java.util.List;

public class Vez extends Piece {

    public Vez(boolean isBlack, Board b){
        super(isBlack,b);
    }

    @Override
    public boolean canMove(int col, int row) {
        int j = col - getField().getCol();
        int i = row - getField().getRow();

        if((i == 0) && (j == 0)){
            return false;
        }

        //ide po riadku alebo stlpci
        if (!((i == 0) || (j == 0))) {
            return false;
        }

        if (col < 0 || col > 7 || row < 0 || row > 7) {
            return false;
        }

        int smer;
        boolean goRow = true;

        // idem po riadku
        if(i==0){
            // idem po stlpci
            goRow = false;
            i = j;
        }

        if(i<0){
            smer = 1;
        }else{
            smer = -1;
        }

        for(i+=smer; i != 0; i+=smer){
            //riadok
            if(goRow){
                int y = getField().getCol();
                int x = getField().getRow() + i;
                if(!(getBoard().getField(x,y).isEmpty())){
                    return false;
                }
            }
            // stlpec
            else{
                int y = getField().getCol() + i;
                int x = getField().getRow();
                if(!getBoard().getField(x,y).isEmpty()){
                    return false;
                }
            }
        }

        if(!(getBoard().getField(row,col).isEmpty())){
            if(isBlack()){
                if(getBoard().getField(row,col).getPiece().isBlack()){
                    return false;
                }
            }else{
                if(!(getBoard().getField(row,col).getPiece().isBlack())){
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public Piece.PieceType getType() {
        return PieceType.VEZ;
    }

    @Override
    public String getStringRepr() {
        if(isBlack()){
            return "♜";
        }
        return "♖";
    }
}
