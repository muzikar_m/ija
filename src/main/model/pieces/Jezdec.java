//Projekt IJA duben 2019 
//Autori:	xtelma00 
//		xmuzik06 

package main.model.pieces;

import main.model.Board;
import main.model.Piece;
import main.model.PossibleMove;

import java.util.ArrayList;
import java.util.List;

import static java.lang.Math.abs;

public class Jezdec extends Piece {
    public Jezdec(boolean isBlack, Board b){
        super(isBlack, b);
    }

    @Override
    public boolean canMove(int col, int row) {
        int j = col - getField().getCol();
        int i = row - getField().getRow();

        if(!((abs(i)==2) && (abs(j)==1))){
            if(!((abs(i)==1 && (abs(j)==2)))){
                return false;
            }
        }

        if (col < 0 || col > 7 || row < 0 || row > 7) {
            return false;
        }

        if(!(getBoard().getField(row,col).isEmpty())){
            if(isBlack()) {
                if(getBoard().getField(row,col).getPiece().isBlack()){
                    return false;
                }
            }else{
                if(!(getBoard().getField(row,col).getPiece().isBlack())){
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public PieceType getType() {
        return PieceType.JEZDEC;
    }

    @Override
    public String getStringRepr() {
        if(isBlack()){
            return "♞";
        }
        return "♘";
    }
}
