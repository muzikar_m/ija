//Projekt IJA duben 2019 
//Autori:	xtelma00 
//		xmuzik06 

package main.model.pieces;

import main.model.Board;
import main.model.Piece;
import main.model.PossibleMove;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Pesec extends Piece {
    public Pesec(boolean isBlack, Board b){
        super(isBlack, b);
    }

    @Override
    public boolean canMove(int col, int row) {
        int j = col - getField().getCol();
        int i = row - getField().getRow();

        if ((i == 0) && (j == 0)) {
            return false;
        }

        int smer = 1;
        if (isBlack()) {
            smer = -1;
        }

        if (col < 0 || col > 7 || row < 0 || row > 7) {
            return false;
        }

        // 1 krok dopredu
        if ((i == smer) && (j == 0) && (getBoard().getField(row,col).isEmpty())) {
            return true;
        }
        // 2 kroky dopredu
        if (((i == 2*smer) && (j == 0) && (getBoard().getField(row,col).isEmpty())) &&
            ((getBoard().getField(row-smer,col).isEmpty()))){
            if(isBlack() && (getField().getRow() == 6)){
                return true;
            }
            if((!(isBlack())) && (getField().getRow()==1)){
                return true;
            }
        }
        //krok diagonalne
        Set<Integer> colSet = new HashSet<>();
        colSet.add(-1);
        colSet.add(1);
        if ((i == smer) && (colSet.contains(j)) && (!(getBoard().getField(row,col).isEmpty()))) {
            if(isBlack()){
                if(getBoard().getField(row,col).getPiece().isBlack()){
                    return false;
                }
            }else{
                if(!(getBoard().getField(row,col).getPiece().isBlack())){
                    return false;
                }
            }
            return true;
        }
        return false;
    }
    
    @Override
    public PieceType getType() {
        return PieceType.PESEC;
    }

    @Override
    public String getStringRepr() {
        if(isBlack()){
            return "♟";
        }
        return "♙";
    }
}
