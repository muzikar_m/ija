//Projekt IJA duben 2019 
//Autori:	xtelma00 
//		xmuzik06 

package main.model.pieces;

import main.model.Board;
import main.model.Piece;
import main.model.PossibleMove;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Kral extends Piece {
    public Kral(boolean isBlack, Board b){
        super(isBlack, b);
    }

    @Override
    public boolean canMove(int col, int row) {
        int j = col - getField().getCol();
        int i = row - getField().getRow();

        if((i == 0) && (j == 0)){
            return false;
        }

        Set<Integer> colSet = new HashSet<>();
        colSet.add(-1);
        colSet.add(0);
        colSet.add(1);
        if (!((colSet.contains(i)) && (colSet.contains(j)))) {
            return false;
        }
        if(col<0 || col>7 || row<0 || row>7){
            return false;
        }

        if(!(getBoard().getField(row,col).isEmpty())){
            if(!(getBoard().getField(row,col).isEmpty())){
                if(isBlack()){
                    return !getBoard().getField(row, col).getPiece().isBlack();
                }else{
                    return getBoard().getField(row, col).getPiece().isBlack();
                }
            }
        }
//        Game game = GameManager.getManager().getCurrentGame();
//        red_fields
        return true;
    }
    
    @Override
    public PieceType getType() {
        return PieceType.KRAL;
    }

    @Override
    public String getStringRepr() {
        if(isBlack()){
            return "♚";
        }
        return "♔";
    }
}
