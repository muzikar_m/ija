//Projekt IJA duben 2019 
//Autori:	xtelma00 
//		xmuzik06 

package main.model.pieces;

import main.model.Board;
import main.model.Piece;
import main.model.PossibleMove;

import java.util.ArrayList;
import java.util.List;

import static java.lang.Math.abs;

public class Strelec extends Piece {

    public Strelec(boolean isBlack, Board b){
        super(isBlack,b);
    }

    @Override
    public boolean canMove(int col, int row) {
        if (col < 0 || col > 7 || row < 0 || row > 7) {
            return false;
        }

        int j = col - getField().getCol();
        int i = row - getField().getRow();

        if((i == 0) && (j == 0)){
            return false;
        }

        // kontrola diagonalneho smeru
        if(abs(i) != abs(j)){
            return false;
        }

        int smerX,smerY;
        if(i<0){
            smerX = 1;
        }else{
            smerX = -1;
        }

        if(j<0){
            smerY = 1;
        }else{
            smerY = -1;
        }

        i += smerX;
        j += smerY;
        while((i != 0) && (j != 0)){
            int y = getField().getCol() + j;
            int x = getField().getRow() + i;
            if(!(getBoard().getField(x,y).isEmpty())){
                return false;
            }
            i += smerX;
            j += smerY;
        }

        if(!(getBoard().getField(row,col).isEmpty())){
            if(isBlack()){
                if(getBoard().getField(row,col).getPiece().isBlack()){
                    return false;
                }
            }else{
                if(!(getBoard().getField(row,col).getPiece().isBlack())){
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public PieceType getType() {
        return PieceType.STRELEC;
    }

    @Override
    public String getStringRepr() {
        if(isBlack()){
            return "♝";
        }
        return "♗";
    }
}
