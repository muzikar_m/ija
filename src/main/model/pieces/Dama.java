//Projekt IJA duben 2019 
//Autori:	xtelma00 
//		xmuzik06 

package main.model.pieces;

import main.model.Board;
import main.model.Piece;
import main.model.PossibleMove;

import java.util.List;

public class Dama extends Piece {
    static Vez stVez = new Vez(false,null);
    static Strelec stStrelec = new Strelec(false,null);

    public Dama(boolean isBlack, Board b){
        super(isBlack, b);
    }

    @Override
    public boolean canMove(int row, int col) {
        stStrelec.setField(getField());
        stVez.setField(getField());
        stStrelec.setBoard(getBoard());
        stVez.setBoard(getBoard());
        stStrelec.setBlack(isBlack());
        stVez.setBlack(isBlack());
        return stStrelec.canMove(row,col) || stVez.canMove(row,col);
    }

    @Override
    public PieceType getType() {
        return PieceType.DAMA;
    }

    @Override
    public String getStringRepr() {
        if(isBlack()){
            return "♛";
        }
        return "♕";
    }
}
