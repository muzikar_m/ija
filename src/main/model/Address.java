//Projekt IJA duben 2019 
//Autori:	xtelma00 
//		xmuzik06 

package main.model;

public class Address {
    int col;
    int row;

    /**
     * Adresa v herni plose (adresovane od 0)
     * @param col sloupec
     * @param row radek
     */
    public Address(int col, int row) {
        this.col = col;
        this.row = row;
    }

    public int getCol() {
        return col;
    }

    public int getRow() {
        return row;
    }
}
