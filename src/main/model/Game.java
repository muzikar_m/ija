//Projekt IJA duben 2019 
//Autori:	xtelma00 
//		xmuzik06 

package main.model;

import javafx.util.Pair;
import main.utils.Parser;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Stream;

public class Game {

    List<Move> moves;
    Board board;
    int currentMove;
    boolean isBlacksTurn;

    public Game() {
        moves = new ArrayList<>();
        board = new Board();
        board.initBoard();
        currentMove = -1;
        isBlacksTurn = false;
    }

    /**
     * Provede pohyb move
     * @param move pohyb ktery chceme provest
     */
    public void doMove(Move move){
        if (isBlacksTurn){
            if (!move.pfrom.isBlack()){
                return;
            }
        } else {
            if (move.pfrom.isBlack()){
                return;
            }
        }
        currentMove++;
        while (currentMove != moves.size()){
            moves.remove(moves.size() - 1);
        }
        isBlacksTurn = !isBlacksTurn;
        moves.add(move);
        move.to.setPiece(move.pfrom);
        move.from.setPiece(null);
    }

    /**
     * Vrati hru o tah zpatky pokud je to mozne
     */
    public void undo(){
        if (currentMove < 0){
            return;
        }
        isBlacksTurn = !isBlacksTurn;
        Move m = moves.get(currentMove);
        m.from.setPiece(m.to.getPiece());
        m.to.setPiece(m.pto);
        currentMove--;
    }

    /**
     * Provede tah pokud je to mozne
     */
    public void redo(){
        if (currentMove + 1 >= moves.size()){
            return;
        }
        isBlacksTurn = !isBlacksTurn;
        currentMove++;
        Move m = moves.get(currentMove);
        m.to.setPiece(m.from.getPiece());
        m.from.setPiece(null);
    }

    /**
     * Nastavi index soucasneho pohybu
     * @param move index
     */
    public void setCurrentMove(int move) {
        if (move > currentMove){
            while (move != currentMove){
                redo();
            }
        } else {
            while (move != currentMove){
                undo();
            }
        }
    }

    /**
     * Nacte pohyby ze souboru
     * @param f soubor ze ktereho se bude cist
     * @throws Exception spatna prace se soborem/spatne formatovana notace
     */
    public void loadFile(File f) throws Exception{
        Parser parser = new Parser(f, board);
        while (parser.hasNext()){
            Pair<Move, Move> move = parser.nextMove();
            if (!move.getKey().isEmpty()){
                doMove(move.getKey());
            }
            if (!move.getValue().isEmpty()){
                doMove(move.getValue());
            }
        }
    }

    /**
     * @return Vraci true pokud je kral v ohrozeni, takze je sach
     */
    public boolean isSach(){
        Optional<Piece> kral = Stream.of(board.getPieces()).filter(piece -> piece.getType() == Piece.PieceType.KRAL && piece.isBlack() == isBlacksTurn()).findFirst();
        if (!kral.isPresent()){
            return false;
        }
        return Stream.of(board.getPieces())
                .filter(piece -> piece.getField() != null) // "Zive" figurky
                .filter(piece -> piece.isBlack() != isBlacksTurn()) // Figurky protihrace
                .anyMatch(piece -> piece.canMove(kral.get().getField().getCol(), kral.get().getField().getRow()));
    }

    public Board getBoard() {
        return board;
    }

    public int getCurrentMove() {
        return currentMove;
    }

    public List<Move> getMoves() {
        return moves;
    }

    public boolean isBlacksTurn() {
        return isBlacksTurn;
    }
}
