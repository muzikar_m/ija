//Projekt IJA duben 2019 
//Autori:	xtelma00 
//		xmuzik06 

package main.model;

import java.util.ArrayList;
import java.util.List;

public class GameManager {

    static GameManager manager;
    List<Game> games;
    int currentGame;

    private GameManager(){
        games = new ArrayList<>();
        currentGame = -1;
    }

    /**
     * @return vraci instanci singletonu
     */
    public static GameManager getManager(){
        if (manager == null)
            manager = new GameManager();
        return manager;
    }

    /**
     * Vytvori novou hru
     * @param game hra
     * @return index hry
     */
    public int newGame(Game game){
        games.add(game);
        currentGame = games.size() - 1;
        return games.size() - 1;
    }

    /**
     * @return aktualni hru
     */
    public Game getCurrentGame(){
        return games.get(currentGame);
    }

    public int getCurrentGameIndex(){
        return currentGame;
    }

    public void setCurrentGame(int id){
        currentGame = id;
    }

}
