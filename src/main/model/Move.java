//Projekt IJA duben 2019 
//Autori:	xtelma00 
//		xmuzik06 

package main.model;

public class Move {

    Field from;
    Field to;

    Piece pfrom;
    Piece pto;

    /**
     * Struktura pohybu
     * @param from policko ze ktereho pohyb probehl
     * @param to policko na ktere se pohyb stal
     * @param pfrom figurka se kterou se tahlo
     * @param pto mozna vyrazena figurka
     */
    public Move(Field from, Field to, Piece pfrom, Piece pto) {
        this.from = from;
        this.to = to;
        this.pfrom = pfrom;
        this.pto = pto;
    }

    /**
     * Pomocna funkce pro rychle generovani Move
     * @param f1 policko Z
     * @param f2 policko KAM
     * @return pohyb
     */
    public static Move fromFields(Field f1, Field f2){
        return new Move(f1, f2, f1.getPiece(), f2.getPiece());
    }

    /**
     * Pomocna funkce pro vytvoreni prazneho poybu
     */
    public static Move makeEmpty(){
        return new Move(null, null, null, null);
    }

    /**
     * @return true pokyd je pohyb prazdny
     */
    public boolean isEmpty(){
        return this.from == null;
    }

    /**
     * @return Plnou notaci pohybu
     */
    public String getFullNotation(){
        return String.format("%c%c%d%c%d", pfrom.getType().n, from.col + 'a', from.row + 1, to.col + 'a', to.row + 1);
    }
}
