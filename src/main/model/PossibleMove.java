//Projekt IJA duben 2019 
//Autori:	xtelma00 
//		xmuzik06 

package main.model;

public class PossibleMove {

    public final MoveType type;
    public final int row, col;

    /**
     * Pohyb ktery muze figurka provest
     * @param type typ pohybu (bude brat figurku nebo se jedna pouze o pohyb)
     * @param row radek na ktery pohyb bude smerovat
     * @param col sloupec na ktery bude pohyb smerovat
     */
    public PossibleMove(MoveType type, int row, int col) {
        this.type = type;
        this.row = row;
        this.col = col;
    }

    public enum MoveType {
        MOVE,
        KILL
    }
}
